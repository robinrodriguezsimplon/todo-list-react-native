import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput, AsyncStorage } from 'react-native';

const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: 80,
    backgroundColor: 'white',
    justifyContent:'center',
    alignItems: "center",
  },
  title: {
    textAlign: 'center',
    fontSize: 40,
    fontWeight: 'bold',
    borderBottomWidth: 2,
    borderBottomColor: 'black',
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  body: {
    backgroundColor: 'white',
    padding: 10,
    width: '80%',
    minHeight: '80%'
  },
  container: {
    flex: 1,
    backgroundColor: 'black',
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hr: {
    height: 1,
    backgroundColor: "gray"
  },
  textInput: {
    backgroundColor: 'gray',
    height: 40,
    paddingRight: 10,
    paddingLeft: 10,
    borderColor: "gray",
    borderWidth: 0,
    width: "50%"
  },
  inputAndBtn: {
    flexDirection: 'row',
  }
});

export default class App extends Component {
  state = {
    tasks: [],
    text: ""
  };

  deleteTask = i => {
    this.setState(
      prevState => {
        let tasks = prevState.tasks.slice();

        tasks.splice(i, 1);

        return { tasks: tasks };
      },
      () => Tasks.save(this.state.tasks)
    );
  };

  changeTextHandler = text => {
    this.setState({ text: text });
  };

  addTask = () => {
    let notEmpty = this.state.text.trim().length > 0;

    if (notEmpty) {
      this.setState(
        prevState => {
          const { tasks, text } = prevState;
          return {
            tasks: tasks.concat({ key: tasks.length, text: text }),
            text: ""
          };
        },
        () => Tasks.save(this.state.tasks)
      );
    }
  };

  componentDidMount() {
    Tasks.all(tasks => this.setState({ tasks: tasks || [] }));
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>Ma liste</Text>
        </View>
        <View style={styles.body}>
          <FlatList 
            data={this.state.tasks}
            renderItem={({ item, index }) =>
            <View key={index}>
              <View style={styles.item}>
                <Text>{`- `+item.text}</Text>
                <Button title="X" onPress={() => this.deleteTask(index)} />
              </View>
              <View style={styles.hr} />
            </View>}
          />
        </View>
        <View style={styles.inputAndBtn}>
          <TextInput
            style={styles.textInput}
            onChangeText={this.changeTextHandler}
            value={this.state.text} 
            placeholder="Ajouter un truc a ma liste"
            returnKeyType="done"
            returnKeyLabel="done"
          />
          <Button title="Ajouter" onPress={this.addTask}/>
        </View>
      </View>
    );
  }
}

let Tasks = {  
  convertToArrayOfObject(tasks, callback) {
    return callback(
      tasks ? tasks.split("||").map((task, i) => ({ key: i, text: task })) : []
    );
  },
  convertToStringWithSeparators(tasks) {
    return tasks.map(task => task.text).join("||");
  },
  all(callback) {
    return AsyncStorage.getItem("TASKS", (err, tasks) =>
      this.convertToArrayOfObject(tasks, callback)
    );
  },
  save(tasks) {
    AsyncStorage.setItem("TASKS", this.convertToStringWithSeparators(tasks));
  }
};